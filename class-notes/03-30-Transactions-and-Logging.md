# Coping with System Failures

*Resilience* is the integrity of the data when the system fails in some way

Principle technique supporting resilience is the log

3 different styles of logging (that we'll discuss):
- Undo
- Redo
- Undo/Redo

## Failures

4 Main failure modes

**Erroneous data entry**

Some are impossible to detect (wrong phone number) 

But generally are avoided using Checks/Constraints

**Media Failures**

Local failures are handled by checksums

Disk failure handled by 
- RAID
- Maintain an archive (periodically updated, stored a safe distance away)
- Redundant copies kept online 

**Catastrophic Failure**

Floods, fires, vandalism

RAID doesn't usually help here 

Other techniques for Media Failures usually are 

**System Failures**

Causes the transaction state to be lost

Typically caused by power failures or software errors

## Transactions

The transaction is the unit of execution of database operations.

It can be ad-hoc (just issuing commands)

Or explicit (`BEGIN`/`ROLLBACK`/`COMMIT`)

A transaction must execute atomically: all or nothing, as if it were done in a moment of time

### Correctness

What does it mean for a transaction to execute correctly?

Assume a database is composed of elements:
- Relations
- Disk Blocks
- Individual Tuples (or objects)

It's helpful to assume Disk Blocks. Elements are move to and from Main Memory as single units. 

A database also has *state*, a value for each of its elements

*Consistent* states satisfy all constraints of the database schema

Fundamental assumption for transactions:

If a transaction executes, in the absence of other transactions or system errors, and it starts with the database in a consistent state, then it will be in a consistent state when it ends.

### Operators

Primitive operators of transactions

3 address spaces to consider:
- Space of disk blocks holding DB element
- Main memory, managed by the buffer manager
- Local address space of the Transaction 

For a transaction to read an element, it must first be brought to main memory, if it's not already there. Then it can be read into the Transaction address space. 

An element may or may not be written to disk immediately. 

We'll define transaction operators:
- Input(X): copy the disk block with X to main memory
- Read(X, t): read X from main memory into transaction variable t
- Write(X, t): writes the value from transaction variable t to X in main memory
- Output(X): writes the memory buffer containing X to disk

Example:

``` 
a := a * 2
b := b * 2

Action      t       MemA    MemB    DiskA   DiskB
--------------------------------------------------
                                    5       8
Input(a)            5               5       8
Input(b)            5       8       5       8
Read(a, t)  5       5       8       5       8
t=t*2       10      5       8       5       8
Write(a,t)  10      10      8       5       8
Read(b, t)  8       10      8       5       8
t=t*2       16      10      8       5       8
Write(b,t)  16      10      16      5       8
Output(a)   16      10      16      10      8
Output(b)   16      10      16      10      16




```

## Logging

A log is a file of log records

Imagine a log as a file opened for append only

As the transaction executes, the Log Manager will record important events 

Log records have several forms:

- Start T: transaction T started
- Commit T: transaction T was committed and will make no further changes to the database (however, we can't enforce this)
- Abort T: transaction T could not complete successfully.
    - It's the job of the transaction manager to make sure the changes of T never appear on disk
- Update<T, X, v>: Transaction T changed X for value v

## Undo Logging

Undo logging repairs database damage by rolling back transactions that didn't complete before the crash. 

Rules:
- If transaction T modifies X, the log <T, X, v> must be written to disk *before* the new value is written to disk 
- If a transaction commits, then its COMMIT record must be written to disk only *after* all its changes have been written to disk

Add an additional Transaction operator: FLUSH LOG

``` 
a := a * 2
b := b * 2

Action      t       MemA    MemB    DiskA   DiskB   Log
-------------------------------------------------------
                                    5       8       Start T
Input(a)            5               5       8
Input(b)            5       8       5       8
Read(a, t)  5       5       8       5       8
t=t*2       10      5       8       5       8
Write(a,t)  10      10      8       5       8       <T, A, 5>
Read(b, t)  8       10      8       5       8
t=t*2       16      10      8       5       8
Write(b,t)  16      10      16      5       8       <T, B, 8>
Flush Log
Output(a)   16      10      16      10      8
Output(b)   16      10      16      10      16
                                                    Commit T
Flush Log

```

### Recovery

Start at the beginning of the log

Ignore all committed transactions

From the end of the log, for every update statement that's part of an uncommitted transaction, replace X with v (the old value)

We add Abort T statement to the log for every uncommitted transaction.


## Checkpointing

Checkpointing keeps the log from growing too big

- Wait for all current transactions to finish
- Stop new transactions from starting
- Flush Log
- Write CHECKPOINT to the log
- Flush Log 
- Resume new Transactions


A drawback to checkpointing is that you have to stop new transactions

*Nonquiescent checkpointing* doesn't require a shutdown of the database.

- Write Start CKPT(Tx1, 2, 3, ... Txk) for all active transactions 
- Wait until every transaction in the list completes 
- End CKPT

### Recovery

- Work backwards, undoing as we go 
- If we encounter END CKPT, we only go back to the previous START CKPT.
- Otherwise, we only go back until we find the start of the earliest transaction in the list.

## Redo Logging

Undo logging has the problem that we can't commit any transaction until all the changes are written to disk.

We may save I/Os if changes are allowed to remain in memory for a while.

Redo logging addresses that problem

Main differences from Undo Logging:
- Redo logging repeats the effects of committed transactions, rather than undoing the not-committed transactions
- Redo Logging requires the `COMMIT` record be written to disk before any data changes are written to disk
- Update records record the new value, instead of the old.

**Redo Logging Rule**: Before writing any change to disk, both the update record and the `COMMIT` record must be written to disk.

``` 
a := a * 2
b := b * 2

Action      t       MemA    MemB    DiskA   DiskB   Log
-------------------------------------------------------
                                    5       8       Start T
Input(a)            5               5       8
Input(b)            5       8       5       8
Read(a, t)  5       5       8       5       8
t=t*2       10      5       8       5       8
Write(a,t)  10      10      8       5       8       <T, A, 10>
Read(b, t)  8       10      8       5       8
t=t*2       16      10      8       5       8
Write(b,t)  16      10      16      5       8       <T, B, 16>
                                                    Commit T
Flush Log
Output(a)   16      10      16      10      8
Output(b)   16      10      16      10      16
                                                    
```

### Recovery

- Ignore incomplete transactions
- Identify committed transactions
- Start at the beginning of the log, and replay the Committed Transactions
- Write `ABORT T` record for each uncommitted transaction

### Checkpointing 

A little more complicated than undo logging: we have to keep track of which buffers in memory are *dirty*

But we can checkpoint without waiting for active transactions to finish, since they can't write anyway.

1. Write START CKPT(T1, T2, ... Tk) (where the Ts are the active transactions)
    - Flush Log
2. Write to disk all the dirty buffers for the completed transactions
3. Write END CKPT record
    - Flush Log

#### Recovery

Two cases, depending on if the last record is START CKPT or END CKPT

- END CKPT: everything prior to the preceding START CKPT was written to disk:
    - Any Transaction listed in that START CKPT record, or after, may have unwritten changes 
    - We can ignore everything before the earliest START Ti record 
- START CKPT: We can't be sure that the transactions committed prior to the checkpoint had their buffers written to disk.
    - We have to search back to the previous END CKPT record, as in the preceding case 

## Undo/Redo Logging

Drawbacks of Undo and Redo logging:
- Undo: data must written to disk immediately, potentially increasing I/Os 
- Redo: must keep all modified blocks in memory until the end of the transaction, increasing memory requirements 
- Both: put contradictory requirements on the buffer manager during checkpointing

**Undo/Redo Logging Rule**: Before modifying any X on disk, the update record <T, X, v, w> must appear on disk

*Note* We have to log both the old and new value in the log

``` 
a := a * 2
b := b * 2

Action      t       MemA    MemB    DiskA   DiskB   Log
-------------------------------------------------------
                                    5       8       Start T
Input(a)            5               5       8
Input(b)            5       8       5       8
Read(a, t)  5       5       8       5       8
t=t*2       10      5       8       5       8
Write(a,t)  10      10      8       5       8       <T, A, 5, 10>
Read(b, t)  8       10      8       5       8
t=t*2       16      10      8       5       8
Write(b,t)  16      10      16      5       8       <T, B, 8, 16>
Flush Log
Output(a)   16      10      16      10      8       
Output(b)   16      10      16      10      16
                                                    Commit T
Flush Log
                                                    
```

### Recovery 

- Redo all the committed transactions, earliest first (start at the beginning of the log)
- Undo all the uncommitted transactions, latest first (from the end of the log)

**Side effect** of the delayed commit: it can lead to wasted work if the transaction was finished but the commit record wasn't flushed to the log. Then the transaction is unnecessarily undone.

Possible solution: **second undo/redo logging rule**: always flush the log immediately after writing the `COMMIT` record

### Checkpointing

- Write START CKPT(T1, T2, ... Tk)
    - Flush Log
- Write *all* dirty buffers to disk
- Write END CKPT
