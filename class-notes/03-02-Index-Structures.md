# Index Structures

Consider `SELECT * FROM R WHERE a=10`

An index is a data structure that takes the value of one or more fields (the search key) and finds the records with tuples containing the key

## Index Structure Basics

Storage structures consist of files, similar to the files used by an operating system

For example, a "data file" may store relations (or the tuples of a relation)

A given data file may have more than one index file

Each index file associates values of the search key with pointers to the data file records that those values. 

Indexes can be:
- Dense: there's an entry for every record of the data file
- Sparse: only some records are represented, often one per block 

Indexes can be:
- Primary: the index file determines the location for the records in the data file 
- Secondary: the index file does not determine the location 

### Sequential Files

A sequential file is created by sorting the tuples of a relation by their primary key. The tuples are stored in order.

It's common to leave space in each block for insertion of new records

### Dense Indexes

If the records are sorted, we can build a dense index, which is a sequence of blocks holding only the keys of the records and pointers to the records. 

The index must be maintained in the same order as the tuples themselves

Generally most useful when the whole index will fit into memory

The number of blocks in the index is small compared to the size of the relation (number of data blocks)

Keys are sorted, so lookup is Log N (Log base 2)

### Sparse Indexes

Sparse Indexes have one key-pointer pair for each block of the data file

Much less space is required 

A sparse index can only be used if the data file is sorted on the search key

### Multiple Levels of Index 

Index files may not all fit in main memory and so might require multiple disk I/Os to retrieve a record 

An index on an index reduces this problem 

Generally, we prefer B-Trees

### Secondary Indexes

Secondary Indexes do not determine the placement of the records in the data file 

It tells us the location of records, which may already have been determined by a primary index 

Secondary Indexes are always dense 

Applications of Secondary Indexes:
- Additional indexes on other search keys 
- Some storage structures (heaps)
- Clustered Files

### Clustered Files

Assume some relation R with a many-to-one relationship with another relation S.

Ex: Grades -> Students 

Grades are frequently retrieved with students. Rather than storing the grade tuples separately, we can store them with the related student tuples.

Then we create a secondary index for Grades

![1](resources/03-02/1.jpg)

### Indirection in Secondary Indexes

If a search key appears n times in the data file, it must also appear n times in the index file.

It could be better to write the value once for all the pointers to records for that value.

We use "buckets" as a level of indirection. Each search key has a pointer to a bucket, and that bucket has pointers to the records for that key.

![2](resources/03-02/2.jpg)

This approach saves space as long as the search key is larger than a pointer, and the average key appears twice.

There are other advantages: we can use the pointers in the buckets to improve the query before we've retrieved any data.

Example: 

```postgresql
SELECT * 
FROM course 
WHERE semester='S20' 
AND location='DCC-318'
AND time='TF10-12'
``` 

We can find the pointers that satisfy all conditions before the data is retrieved

### Document Retrieval and Inverted Indexes

Common challenge to retrieve documents that contain a given keyword

Think of a document as a tuple in a relation (Doc). Doc has many attributes, one for each possible word. Each attribute is boolean (either the document contains the word or it does not). 

Build a secondary index on each attribute of Doc. However, we don't store pointers to records where the value is False.

Instead of having separate indexes, we create one large index, called an inverted index. 

We use buckets for space efficiency

We can expand the index to consider other contextual information

This allows more complex queries without actually retrieving any data, as we can again compare sets of pointers. 

![3](resources/03-02/3.jpg)


 ## B-Trees
 
 B-Trees are a family of data structure used for index (we'll limit ourselves to B+ Trees)
 
 B-Trees automatically maintain as many levels of index as is appropriate for the size of the file being indexed
 
 B-Trees manage the space on the blocks they use so that every block is between half used and full. 
 
 B-Trees organize their blocks so that the tree is balanced: all paths from root to leaf have the same length
 - Typically three layers 
 
 There is a parameter n that is associated with a given B-Tree. Each block will have space for n search-key values and n+1 pointers.
 
 We pick n to be as large as possible within the given constraints
 
 Example: Block size of 4096 bytes, Integer search keys (4 bytes), 8-byte pointers 
 
 4n + 8(n+1) <= 4096
 
 n = 340
 
 ### B-Tree Blocks
 
 What's in a B-Tree Block?
 - Keys in leaf nodes are copies of keys from the data file
    - They are distributed among the leaves in sorted order
 - At the root, there are at least two used pointers
    - All pointers point to blocks at the level below
 - At a leaf, the last pointer points to the next leaf block
    - All other used pointers (at least (n+1)/2 of them) point to data records
    - The ith pointer points to a record containing the ith search key value
- At an interior node, all n+1 pointers can be used to point to blocks at the next level
    - At least (n+1)/2 pointers are used
    - If j pointers are used, there will be j-1 keys: k1, k2, ..., k(j-1)
        - The first pointer points to a part of the tree with values less than k1
        - The second pointer points to a part of of the tree with values greater than or equal to k1, and less than k2
        - Etc. 
        - The last pointer points to a part of the tree with values greater than k(j-1) 
- All used pointers and their key appear the beginning of the block, except the (n+1)th pointer in a leaf, which points to the next leaf

![4](resources/03-02/4.jpg)

### Applications of B-Trees

- Search key of the B-Tree is the primary key for the data file, the index is dense . One key-pointer pair for every record in the data file
- The data file is sorted by its primary key. The B-Tree is a sparse index: one key-pointer pair for every block the data file
- The data file is sorted by an attribute that is not a key, and this attribute is a search key for the B-Tree
    - One key-pointer pair for every value k that appears. The pointer points to the first instance of k. 

Note that we can have B-Trees that allow for multiple instances of a search key. In such cases, the key values in each node might not all be filled.

For the purposes of this class, we'll assume no duplicates. Also assume a dense index: every search key value in the data file also appears at a leaf.

### Operations in B-Trees

**Lookup** is recursive: Find search key k.

Base case: If we're at a leaf node, if the ith key is k, the ith pointer takes us to the correct record

Induction: Interior node with keys k1, k2, ... kn
- If k < k1, follow the 1st pointer
- If k >= k1, and k < k2, follow the second pointer
- If k < ki, follow ith pointer
- Else follow n+1 pointer

**Range Queries**

B-Trees are useful not only for single value, but for range queries as well (between a and b)

Search for a. Whether or not it exists, we end up at the leaf node where it would.

We follow every associated pointer, using the n+1 pointer to jump to the next leaf, until a key larger than b, which point we stop. 

**Insertion** is a recursive process

Find the appropriate leaf, and insert if there's room

If there's no space, split the leaf, and divide the contents equally. Splitting appears as insertion at the next level, so repeat. 

The only exception is if Root is full, in which case we split and create a new root at the next level. 

When we split a node, we need to manage the keys. 

We now have n+1 key values.

We create a new node, to the right of the current node

We take the first (n+1)/2 keys and pointers and leave them in the existing node

We take the last (n+1)/2 keys and pointers, and move them to the new node 

We take the existing n+1 pointer, and move it to the new node, and replace it with a pointer to the new node

**Deletion**

We do a search, and delete the key-pointer pair

However, it's possible that the deletion drops the space used below the 50% threshold. If an adjacent node as more than the minimum, we move a key from there. Otherwise, we can combine the two to create a full node.

### Efficiency of B-Trees

B-Trees typically have 3 levels (340 gives ~16 million records)

Lookup requires 3 reads (2 if the root is buffered) 

Most inserts and deletes only require the same 