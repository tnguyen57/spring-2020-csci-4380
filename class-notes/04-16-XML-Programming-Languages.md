# Programming Languages for XML

3 Basic Languages:
- XPath: simple language for describing sets of paths
- Xquery: an extension of XPath, providing some SQL-like capabilities
- XSLT: Extensible Stylesheet Language for Transformation
    - Allows creation of templates/stylesheets that define how to process an XML document
    - Turing complete
    - Declarative, not procedural 

## XPath

XPath is a data model

In the relational model, everything is a bag of tuples 

In XPath, everything is a sequence of items

An item is either:
1. A value of primitive type (integer, float, boolean, string, etc.)
2. A node. Many types of nodes; we'll focus on three:
    - Documents: files containing XML (local path/URL)
    - Elements: XML elements, including opening and closing tags, and everything in between
    - Attributes: found inside XML tags for elements
    
Items of a sequence do not all need to be of the same type (often are)

### Document Nodes

Every XPath query refers to a document

We sometimes use the expression `doc(location)`, where the location is a filesystem path or URL. 

### XPath Expressions

Expressions typically start at the document root and give a sequence of tags and slashes:

`T1/T2/T3`

`/course-data/courses/course/time`

We process the expression in order from left to right, each step working with the output of the previous step (similar to relational algebra)

To process Ti:
1. start with the sequence of items that was the result of Ti-1
2. Examine each item in order
3. Accept all items whose tag matches Ti

Special case: `/T1` will give the single root element. This is distinct form a single document node. 

**Relative Paths** XPath expressions can be relative to the current node (or a sequence of nodes). Relative expressions don't start with a slash

**Attributes** in XPath expressions are denoted by `@`: `/T1/T2.../Tn/@A`

#### Axes

So far, we've only navigated from a node to its child or attribute. 

XPath provides a number of axes for travel, two of which are child and attribute

`/child::T1/child::T2/.../child::Tn/attribute::A`

Other axes:
- Child (default)
- attribute (@)
- parent (..)
- ancestor 
- descendant
- next-sibling
- previous-sibling
- self
- self-or-descendant (//)

#### Conditions in XPath Expressions

We can follow a tag with square brackets enclosing a condition (a boolean expression). That limits the XPath to only tags that meet the condition

Operators: =, !=, <, >, <=, >=, OR, AND

Other useful conditions: [i] is the ith child of its parent 

A tag [T] by itself is true only for elements that have one or more sub-elements of type T

`//course[@semester]`

Attribute by itself [@A] is similar

There are also functions like `last()` `position(n)`





## XQuery

XQuery is a functional language that's an extension of XPath

Consists of FLWR statements: For, Let, While, Return 

``` 
let $courses := doc('courses.xml')
for $course in $courses
where $course/@semester = 'S20'
return $course/@name 
```

## XSLT

eXtensible Stylesheet Language for Transformations

Declarative language (turing complete)

``` 
<xsl:template match="XPath Expression">
    Some output
    <xsl:valueOf select="XPath Expressions"/>
</xsl:template>

<xsl:template match="/course-data/courses/course">
    Name: <xsl:valueOf select="@name"/>
</xsl:template>
```