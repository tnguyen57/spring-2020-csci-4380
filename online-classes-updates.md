# Moving to Online Classes

As we move to online classes, I'll be changing the structure of the course somewhat. I feel it's especially important because we're not creating an online course from the ground up, and with everything else going on, none of us are going to be able to give an online course the attention we would have been able in previous semesters. Furthermore, I have no experience teaching an online course, and I suspect that most of you have no experience taking an online course, so we're all adapting in that respect as well.

## Course Format

Given the chaotic nature of things these days, the course will move to an asynchronous format. The course schedule will be updated to include links for recorded lectures on the topics for the given class. I will make an attempt to publish the notes for the lecture I would have given under normal circumstances a day or two ahead. The recorded lectures may or may not line up exactly. Some will be from Professor Adali's class last semester, while others will be either my own or possibly other freely available content. The recorded lectures will be focused on that part of the material that I feel warrants additional explanation.

The regularly scheduled lecture periods (Mondays and Thursdays 4:00-5:50pm EDT) will be used as extended question and answer periods, using WebEx. I will publish a link for that on Piazza. Attendance is purely optional, at your own discretion. 

## Office Hours

TA's, mentors, and I will continue to hold our regularly scheduled office hours. We will make use of the new Queue feature in Submitty, which will allow you to enter your contact information into a queue to receive an answer from whoever's hosting the office hours. RPI students are all able to host their own WebEx meeting, and I recommend you make use of that capability for office hours by hosting a meeting and providing a link to that meeting as your contact information in the Submitty queue. Of course, you're free to do whatever works best under the circumstances. 

## Changes to Assessment Measures and Grading Criteria

Given that the remainder of the course will be online only, I will not be administering the remaining midterm and final exams. They will be replaced with the following:
- a "Take-home" final worth 10% of your total grade 
    - Anticipate it being roughly the size and scope of two homeworks
    - Individual effort (no collaboration)
    - It will be comprehensive, though given when it will be assigned and due, it won't cover the last few topics
- a group project worth 30% of your final grade
    - Groups of four or five
    - Will involve aspects of various parts of the course

There will now be only six homeworks (the assignments that would have been homeworks 5 and 6 will no longer be given), and homework will make up 35% rather than 40% of the total grade.

## Flexibility

Above all, this is going to require all of us to be flexible. I understand that this is a new situation for all of us, and it's not an easy one. My focus is on helping you learn the material. Please reach out to me or the TAs if you're struggling with the material, or especially if something is making it difficult for you to work on the course. To that end, I'm increasing the number of total late days available to 10, though I'll continue to limit how many may be used on each assignment.

Most deadlines will be flexible, but please don't take advantage of that. There are 164 of you and only one of me. If you leave too many things until the end, I may not be able to get to it before grades are due (a deadline which is probably *not* flexible).

Also, if you find the format or structure of things really isn't working for you, please let me know, and I'll do my best to figure out an alternative.